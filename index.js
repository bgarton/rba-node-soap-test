const soapRequest = require('easy-soap-request');
const fs = require('fs');
const colors = require('colors');
const inquirer = require('inquirer');
const xmlformat = require('xml-formatter');
const path = require('path');
const ini = require('ini');

function getFiles() {
  const dirpath = "./requests/";
  fs.readdir(dirpath, function (err, files) {
    var EXTENSION = '.xml';
    var choiceArray = files.filter(function (file) {
      return path.extname(file).toLowerCase() === EXTENSION;
    });
    inquire(choiceArray);
  })
}

function manageErr(err) {
  console.log("Manager")
  switch (err.code) {
    case "ECONNREFUSED":
      console.log("Is your SOAP server running? Is your IP address and port correct?")
    default:
      console.log(err.code)
      return;
  }
}

async function sendRequest(xmlData) {
  // Concurrent loops - asynchronous
  const NS_PER_SEC = 1e9;
  const MS_PER_NS = 1e-6;
  var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8')).default
  let soapUrl = "http://" + config.soapIP;
  let loopCount = parseInt(config.loopCount);
  let timeoutTime = parseInt(config.timeoutMs);
  let reqHeaders = {
    'user-agent': 'JS/SOAP',
    'Content-Type': 'text/xml;charset=UTF-8',
    'soapAction': soapUrl,
  };

  for (i = 0; i < loopCount; i++) {
    let time = process.hrtime();

    soapRequest({ url: soapUrl, headers: reqHeaders, xml: xmlData, timeout: timeoutTime })
      .then((res) => {
        console.log("Success! Server response code:", res.response.statusCode);
        let diff = process.hrtime(time);
        let timeResult = `${Math.floor((diff[0] * NS_PER_SEC + diff[1]) * MS_PER_NS)} milliseconds`;
        console.log(timeResult,"\r\n");
        //config.logType
        str = `${new Date().toISOString().replace("T","\r\n").slice(0,20)}\r\n${"=".repeat(10)}\r\n${res.response.body}\r\nTime taken: ${timeResult}\r\n\r\n`;
        if (config.logType === "clear") {
          fs.writeFileSync('log.txt', str);
        } else {
          fs.appendFileSync('log.txt', str);
        }
      }).catch(manageErr)
  }
}

function inquire(choiceArray) {
  inquirer
    .prompt([
      {
        type: "rawlist",
        name: "soapcall",
        message: "Which SOAP request?",
        //default: "RBA_AddWebProducts",
        choices: choiceArray
      }
    ])
    .then(answers => {
      const xmlData = fs.readFileSync(`requests/${answers.soapcall}`, 'utf-8');
      //console.log(xmlData)
      sendRequest(xmlData);
    })
    .catch(error => {
      if (error.isTtyError) {
        // Prompt couldn't be rendered in the current environment
        console.log("Prompt couldn't be rendered in the current environment...");
        console.log(error);
      } else {
        // Something else when wrong
        manageErr(error);
      }
    });
}

//init
getFiles();
