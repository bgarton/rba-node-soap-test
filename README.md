# RBA - SOAP Test with NodeJS
#### This script will automatically poll the /requests/ folder for .xml files and allows the user to execute a SOAP call with them.
1. In the directory, firstly run `npm install`.

2. After this, rename `config.example.ini` to `config.ini` and edit as necessary.

3. Then simply run `node .` and it will execute the script. You will be given a choice of which XML file to send, these are located within the /requests/ directory.